using System.Diagnostics;
using System.Linq;
using System;
using NUnit.Framework;
using FluentAssertions;
using System.Collections.Generic;
using Interview.Exceptions;

namespace Interview.UnitTests
{
    [TestFixture]
    public class InMemoryRepositoryTests
    {

        //Service Under Test
        private InMemoryRepository<TestStorable> _sut;
        private IEnumerable<TestStorable> _initialCollection;

        [SetUp]
        public void SetUp()
        {
            _initialCollection = new[] { new TestStorable() , new TestStorable(), new TestStorable() };
            _sut = new InMemoryRepository<TestStorable>(_initialCollection);
        }

        [Test]
        public void All_ShouldReturnAllElements()
        {
            //Arrange
            //Act
            var result = _sut.All();

            //Assert
            result.SequenceEqual(_initialCollection);
        }

        [Test]
        public void All_WithEmptyCollection_ShouldReturnEmptyCollection()
        {
            //Arrange
            _sut = new InMemoryRepository<TestStorable>();
            //Act
            var result = _sut.All();

            //Assert
            result.SequenceEqual(new TestStorable[] { });
        }

        [Test]
        public void FindById_ShouldReturnExpectedItem()
        {
            //Arrange
            var itemToRetrieve = _initialCollection.Skip(1).First();
            //Act
            var result = _sut.FindById(itemToRetrieve.Id);

            //Assert
            result.Should().Be(itemToRetrieve);
        }

        [Test]
        public void FindById_WithIncorrectId_ShouldThrowNotFoundException()
        {
            //Arrange
            var idGuid = Guid.NewGuid();

            //Act / Assert
            Assert.Throws<ItemNotFoundException>(
                () => { _sut.FindById(idGuid); });
        }

        [Test]
        public void Delete_ShouldDeleteItem()
        {
            //Arrange
            var itemToDelete = _initialCollection.Skip(1).First();

            //Act
            _sut.Delete(itemToDelete.Id);

            //Assert
            Assert.Throws<ItemNotFoundException>(
                () => { _sut.FindById(itemToDelete.Id); });
        }

        [Test]
        public void Delete_AlreadyDeletedItem_ShouldThrowItemNotFoundException()
        {
            //Arrange
            var itemToDelete = _initialCollection.Skip(1).First();
            _sut.Delete(itemToDelete.Id);

            //Act / Assert
            Assert.Throws<ItemNotFoundException>(
                () => { _sut.Delete(itemToDelete.Id); });
        }

        [Test]
        public void Save_ShouldInsertItem()
        {
            //Arrange
            var toStore = new TestStorable();

            //Act
            _sut.Save(toStore);

            //Assert
            var result = _sut.All();
            result.SequenceEqual(_initialCollection.Union(new[] { toStore}));
        }

        [Test]
        public void Save_AlreadyPresent_ShouldThrowItemAlreadyExistsException()
        {
            //Arrange
            var toStore = new TestStorable() { TestProperty = "abc" };
            _sut.Save(toStore);

            //Act /Assert
            Assert.Throws<ItemAlreadyExistsException>(
                () => { _sut.Save(toStore); });
        }
    }
}