﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interview.UnitTests
{
    public class TestStorable : IStoreable
    {
        public TestStorable()
        {
            Id = Guid.NewGuid();
        }

        public IComparable Id { get; set; }

        public string TestProperty { get; set; }
    }
}
