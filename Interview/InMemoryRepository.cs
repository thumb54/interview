﻿using Interview.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interview
{
    public class InMemoryRepository<T> : IRepository<T> where T : IStoreable
    {
        Dictionary<IComparable, T> dic = new Dictionary<IComparable, T>();

        public InMemoryRepository()
        { }

        public InMemoryRepository(IEnumerable<T> initialItems)
        {
            if (initialItems == null)
                return;

            foreach (var item in initialItems)
            {
                dic.Add(item.Id, item);
            }
        }

        public IEnumerable<T> All()
        {
            return dic.Values;
        }

        public void Delete(IComparable id)
        {

            if (!dic.ContainsKey(id))
                throw new ItemNotFoundException();

            dic.Remove(id);
        }

        public T FindById(IComparable id)
        {
            if (!dic.ContainsKey(id))
                throw new ItemNotFoundException();

            return dic[id];
        }

        public void Save(T item)
        {
            if (dic.ContainsKey(item.Id))
                throw new ItemAlreadyExistsException();

            dic.Add(item.Id, item);
        }
    }
}
